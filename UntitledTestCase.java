package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class UntitledTestCase {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testUntitledTestCase() throws Exception {
    driver.get("https://simon.inder.gov.co/registro");
    driver.findElement(By.xpath("//SPAN[@id='select2-chosen-15']")).click();
    driver.findElement(By.xpath("//*[@id=\"select2-results-15\"]/li[1]")).click();
    driver.findElement(By.xpath("//*[@id=\"select2-chosen-16\"]")).click();
    driver.findElement(By.xpath("//*[@id=\"select2-results-16\"]/li[1]")).click();
    driver.findElement(By.xpath("//*[@id=\"numeroidentificacion\"]")).click();
    driver.findElement(By.xpath("//*[@id=\"numeroidentificacion\"]")).clear();
    driver.findElement(By.xpath("//*[@id=\"numeroidentificacion\"]")).sendKeys("15508555");
    driver.findElement(By.xpath("//*[@id=\"nombres\"]")).click();
    driver.findElement(By.xpath("//*[@id=\"nombres\"]")).clear();
    driver.findElement(By.xpath("//*[@id=\"nombres\"]")).sendKeys("Jorge");
    driver.findElement(By.xpath("//*[@id=\"apellidos\"]")).click();
    driver.findElement(By.xpath("//*[@id=\"apellidos\"]")).clear();
    driver.findElement(By.xpath("//*[@id=\"apellidos\"]")).sendKeys("Soto");
    driver.findElement(By.xpath("//*[@id=\"select2-chosen-17\"]")).click();
    driver.findElement(By.xpath("//*[@id=\"select2-results-17\"]/li[2]")).click();
    driver.findElement(By.xpath("//*[@id=\"fechanacimiento\"]")).click();
    driver.findElement(By.xpath("//*[@id=\"fechanacimiento\"]")).clear();
    driver.findElement(By.xpath("//*[@id=\"fechanacimiento\"]")).sendKeys("12101982");
    driver.findElement(By.xpath("//*[@id=\"clave_uno\"]")).click();
    driver.findElement(By.xpath("//*[@id=\"clave_uno\"]")).clear();
    driver.findElement(By.xpath("//*[@id=\"clave_uno\"]")).sendKeys("Jsoto0102+");
    driver.findElement(By.xpath("//*[@id=\"clave_dos\"]")).click();
    driver.findElement(By.xpath("//*[@id=\"clave_dos\"]")).clear();
    driver.findElement(By.xpath("//*[@id=\"clave_dos\"]")).sendKeys("Jsoto0102+");
    driver.findElement(By.xpath("//*[@id=\"select2-chosen-19\"]")).click();
    driver.findElement(By.xpath("//*[@id=\"select2-results-19\"]/li[97]")).click();
    driver.findElement(By.xpath("//*[@id=\"select2-chosen-22\"]")).click();
    driver.findElement(By.xpath("//*[@id=\"select2-results-22\"]/li[2]")).click();
    driver.findElement(By.xpath("//*[@id=\"select2-chosen-28\"]")).click();
    driver.findElement(By.xpath("//*[@id=\"select2-results-28\"]/li[3]")).click();
    driver.findElement(By.xpath("//*[@id=\"correoelectronico\"]")).click();
    driver.findElement(By.xpath("//*[@id=\"correoelectronico\"]")).clear();
    driver.findElement(By.xpath("//*[@id=\"correoelectronico\"]")).sendKeys("Jsoto@sispos.com");
    driver.findElement(By.xpath("//*[@id=\"telefonomovil\"]")).click();
    driver.findElement(By.xpath("//*[@id=\"telefonomovil\"]")).clear();
    driver.findElement(By.xpath("//*[@id=\"telefonomovil\"]")).sendKeys("2890102");
    driver.findElement(By.xpath("//*[@id=\"formatoAlto2\"]/div/div[7]/div/ins")).click();
    driver.findElement(By.xpath("//*[@id=\"registro_save\"]")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
